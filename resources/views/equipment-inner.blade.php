@extends('layouts.app')
@section('content')
<div class="equipment-inner">
    <div class="container">
        <div class="equipment-inner__image">
            <img src="{{asset('storage/'.$equipment->thumbnail)}}" alt="">
        </div>
        <div class="equipment-inner__wrapper">
        <h1 class="equipment-inner__title title">{{$equipment->name}}</h1>
        <div class="equipment-inner__descr">
            {!! $equipment->description !!}
        </div>

        <div class="equipment-inner__blueprints blueprints">
            <h2 class="blueprints__title title">Чертежи</h2>
            <div class="blueprints__row">
                @foreach(json_decode($equipment->blueprints) as $blueprint)
                <div class="blueprints__column">
                    <div class="blueprints__image">
                        <img src="{{asset('storage/'.$blueprint)}}" alt="">
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </div>
</div>
@endsection
