@extends('layouts.app')
@section('content')
    <div class="ours">
        <div class="container">
            <div class="ours__banner">
                @if(setting('site.about_image'))
                    <img src="{{asset('storage/'.setting('site.about_image'))}}" alt="">
                    @else
                <img src="images/news/banner.jpg" alt="Баннер">
                    @endif
            </div>
            <h1 class="ours__title">О нас</h1>
            {!! setting('site.about') !!}
        </div>
    </div>
@endsection
