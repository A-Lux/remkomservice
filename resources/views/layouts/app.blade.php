<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ремком Сервис</title>
    <link rel="stylesheet" href="/css/jquery.fancybox.css">
    <link rel="stylesheet" href="/css/style.min.css">
    <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
</head>
<header class="header">
    <div class="container">
        <div class="header__burger">
            <span></span>
        </div>
        <div class="header__row">
            <a href="#" class="header__logo"><img src="/images/icons/Group.svg" alt="Ремком Сервис"></a>
            <nav class="header__nav">
                <ul class="header__list">
                    <li><a href="/">Главная</a></li>
                    <li><a href="{{route('services')}}">Услуги</a></li>
                    <li><a href="{{route('objects')}}">Объекты</a></li>
                    <li><a href="{{route('equipment')}}">Оборудование</a></li>
                    <li><a href="{{route('news')}}">Новости</a></li>
                </ul>
            </nav>
            <div class="header__contacts">
                <a href="{{route('ours')}}" class="header__links">О нас</a>
                <a href="{{route('contacts')}}" class="header__links">Контакты</a>
            </div>
        </div>
    </div>
</header>
@yield('content')
<footer class="footer">
    <div class="container">
        <div class="footer__row">
            <div class="footer__logo">
                <img src="/images/icons/Group.svg" alt="">
            </div>
            <div class="footer__wrapper">
                <ul class="footer__menu">
                    <li><a href="{{route('ours')}}" class="footer__link">О нас</a></li>
                    <li><a href="{{route('objects')}}" class="footer__link">Объекты</a></li>
                    <li><a href="{{route('equipment')}}" class="footer__link">Оборудования</a></li>
                    <li><a href="{{route('news')}}" class="footer__link">Новости</a></li>
                    <li><a href="{{route('services')}}" class="footer__link">Услуги</a></li>
                </ul>
                <div class="footer__bottom">
                    <div class="footer__item"><p>РемКом Сервис</p> Казахстан, г. Алматы. Ул. Суюнбая 43,</div>
                    <div class="footer__item"><a href="tel:+77021750220">Telephone: +7 702 175 0220</a></div>
                    <div class="footer__item">E-mail: remcom@gmail.com</div>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="{{asset('js/main.js')}}"></script>
<body>
