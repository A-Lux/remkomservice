@extends('layouts.app')
@section('content')
    <div class="carousel">
        <div class="container">
            <div class="carousel__inner">
                @foreach($banners as $banner)
                <div>
                    <img class="carousel__image" src="{{asset('/storage/'.$banner->image)}}" style="height: 500px;" alt="">
                    <div class="carousel__text" style="bottom: 70%;">{{$banner->title}}</div>
                </div>

                @endforeach
                @if(count($banners)==0)
                    <div><img class="carousel__image" src="/images/1.jpg" alt=""></div>
                    <div><img class="carousel__image" src="/images/1.jpg" alt=""></div>
                    <div><img class="carousel__image" src="/images/1.jpg" alt=""></div>
                @endif
            </div>
        </div>
    </div>
    <section class="advantages">
        <div class="container">
            <h2 class="advantages__title title">Виды деятельности</h2>
            <div class="advantages__row">
                @foreach($advantages as $advantage)
                <div class="advantages__column">
                    <div class="advantages__item item">
                        <div class="item__row">
                            <div class="item__img">
                            <img src="{{$advantage->icon_title != null ? asset('storage/'.$advantage->icon_title) : '/images/icons/language_24px.svg'}}" alt="" class="item__icon lazy" style="height: 120px;height: 120px;">
                        </div>
                            <div class="item__descr">
                               {{substr($advantage->text,0,100)}}
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @if(count($advantages) == 0)
                <div class="advantages__column">
                    <div class="advantages__item item">
                        <div class="item__row">
                            <div class="item__img">
                            <img src="/images/icons/language_24px.svg" alt="" class="item__icon lazy">
                        </div>
                            <div class="item__descr">Die Pillmeier GmbH ist ein auf Garten- und Landschaftsbau spezialisiertes Unternehmen <br> in Abensberg.</div>
                        </div>
                    </div>
                </div>
                <div class="advantages__column">
                            <div class="advantages__item item">
                                <div class="item__row">
                                    <img src="/images/icons/language_24px.svg" alt="" class="item__icon lazy">
                                    <div class="item__descr">Die Pillmeier GmbH ist ein auf Garten- und Landschaftsbau spezialisiertes Unternehmen <br> in Abensberg.</div>
                                </div>
                            </div>
                        </div>

                @endif
            </div>
        </div>
    </section>
    <div class="carriers">
        <div class="container">
            <h2 class="carriers__title title">Расчет стоимости видов Энергоносителей</h2>
            <div class="carriers__row">
                <div class="carriers__column">
                    <div class="carriers__carrier carrier">
                        <div class="carrier__row">
                            <div class="carrier__img">
                                <img src="/images/icons/3.png" alt="">
                            </div>
                            <ul class="carrier__text">
                                <li class="carrier__item">Пропанобутановая смесь СПБТ: (СУГ) 11 000 Ккал или – 12.7 кВт*ч.</li>
                                <li class="carrier__item">Стоимость 1 кг. СУГ – <span>130</span> тг.</li>
                                <li class="carrier__item">Стоимость 1кВт*ч энергии – <span>10.2</span> тг.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="carriers__column">
                    <div class="carriers__carrier carrier">
                        <div class="carrier__row">
                            <div class="carrier__img">
                                <img src="/images/icons/2.png" alt="">
                            </div>
                            <ul class="carrier__text">
                                <li class="carrier__item">Мазут: 9 700 Ккал или  – 11.2 кВт*ч.</li>
                                <li class="carrier__item">Стоимость 1 литра мазута – <span>150</span> тг.</li>
                                <li class="carrier__item">Стоимость 1 кВт*ч энергии  – <span>13.3</span> тг.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="carriers__column">
                    <div class="carriers__carrier carrier">
                        <div class="carrier__row">
                            <div class="carrier__img">
                                <img src="/images/icons/2.png" alt="">
                            </div>
                            <ul class="carrier__text">
                                <li class="carrier__item">Дизтопливо: 10 300 Ккал или 11,9 кВт*ч.</li>
                                <li class="carrier__item">Стоимость 1 литра солярки – <span>185</span> тг.</li>
                                <li class="carrier__item">Стоимость 1кВт*ч энергии – <span>15,5</span> тг.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="objects">
        <div class="container">
            <h2 class="objects__title title">Объекты</h2>
            <div class="objects__row">
                @foreach($objects as $object)
                    <div class="objects__column">
                        <div class="objects__card card">
                            <a href="#" class="card__row">
                                <div class="card__img">
                                <img src="{{asset('/storage/'.$object->thumbnail)}}" alt="" class="card__square">
                            </div>
                                <div class="card__text">
                                    <div class="card__title">{{$object->name}}</div>
                                    <div class="card__descr">
                                        {!! $object->description !!}
                                    </div>
                                    <div class="card__date date">{{$object->created_at->month.'.'.$object->created_at->day.'.'.$object->created_at->year}}</div>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
                @if(count($objects) == 0 )
                <div class="objects__column">
                    <div class="objects__card card">
                        <a href="#" class="card__row">
                            <img src="/images/square2.jpg" alt="" class="card__square">
                            <div class="card__text">
                                <div class="card__title">People Like Meghan McCain <br>
                                    Are Why Americans Keep Dying</div>
                                <div class="card__descr">
                                    Why we want to believeWhy we want to believe Why we want to believe Why we believe Why we want to believeWhy we...
                                </div>
                                <div class="card__date">Sep 8 2020</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="objects__column">
                    <div class="objects__card card">
                        <a href="#" class="card__row">
                            <img src="/images/square3.jpg" alt="" class="card__square">
                            <div class="card__text">
                                <div class="card__title">People Like Meghan McCain <br>
                                    Are Why Americans Keep Dying</div>
                                <div class="card__descr">
                                    Why we want to believeWhy we want to believe Why we want to believe Why we believe Why we want to believeWhy we...
                                </div>
                                <div class="card__date">Sep 8 2020</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="objects__column">
                    <div class="objects__card card">
                        <a href="#" class="card__row">
                            <img src="/images/square1.jpg" alt="" class="card__square">
                            <div class="card__text">
                                <div class="card__title">People Like Meghan McCain <br>
                                    Are Why Americans Keep Dying</div>
                                <div class="card__descr">
                                    Why we want to believeWhy we want to believe Why we want to believe Why we believe Why we want to believeWhy we...
                                </div>
                                <div class="card__date">Sep 8 2020</div>
                            </div>
                        </a>
                    </div>
                </div>
                @endif
            </div>
            <a href="{{route('objects')}}" class="all">Смотреть все</a>
        </div>
    </section>
    <div class="services-main">
        <div class="container">
            <div class="services-main__bigcard bigcard">
                <div class="bigcard__row">
                    <div class="bigcard__left">
                        <div class="bigcard__title">Услуги</div>
                        <div class="bigcard__descr">
                            Just send your design files to our developer team in the file format of your choice: Photoshop, Sketch, Adobe XD, or even Figma.
    Just send your design files to our developer team in the file
                        </div>
                        <a href="/services"><button class="bigcard__btn">Перейти</button></a>
                    </div>
                    <div class="bigcard__right">
                        <div class="bigcard__img">
                            <img src="/images/main.jpg" alt="Услуги">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="news">
        <div class="container">
            <h2 class="news__title title">Новости</h2>
            <div class="news__row">
                @foreach($news as $new)
                <div class="news__column" style="display: flex">
                    <div class="news__post post" style="display: flex; flex-direction: column;" >
                            <div class="post__image ">
                                <img src="{{asset('storage/'.$new->thumbnail)}}" class="lazy" alt="">
                            </div>
                            <div class="post__content">
                                <div class="post__title">{{$new->title}}</div>
                                <div class="post__descr">
                                    {!! $new->description !!}
                                </div>

                                <div class="post__date date">{{$new->created_at->month.'.'.$new->created_at->day.'.'.$new->created_at->year}}</div>
                            </div>
                    </div>
                </div>
                @endforeach
                @if(count($news) == 0)
                <div class="news__column">
                    <div class="news__post post">
                            <div class="post__image">
                                <img src="/images/Rectangle 5-2.png" class="lazy" alt="">
                            </div>
                            <div class="post__content">
                                <div class="post__title">People Like Meghan McCain
                                    Are Why Americans Keep Dying</div>
                                <div class="post__descr">Why we want to believeWhy we want to believeWhy we want to believeWhy we believeWhy we want to believeWhy we...</div>
                                <div class="post__date date">Sep 8 2020</div>
                            </div>
                    </div>
                </div>
                <div class="news__column">
                    <div class="news__post post">
                        <a href="#" class="post__row">
                            <div class="post__image">
                                <img src="/images/Rectangle 5-3.png" class="lazy" alt="">
                            </div>
                            <div class="post__content">
                                <div class="post__title">People Like Meghan McCain
                                    Are Why Americans Keep Dying</div>
                                <div class="post__descr">Why we want to believeWhy we want to believeWhy we want to believeWhy we believeWhy we want to believeWhy we...</div>
                                <div class="post__date date">Sep 8 2020</div>
                            </div>
                        </a>
                    </div>
                </div>
                @endif
            </div>
            <a href="{{route('news')}}" class="all">Смотреть все</a>
        </div>
    </section>
    <section class="reviews">
        <div class="container">
            <h2 class="reviews__title title">Лицензии Сертификаты</h2>
            <div class="reviews__container">
                <div class="reviews__row">
                    @foreach($reviews as $review)
                    <div class="reviews__column">
                        <div class="reviews__cert cert">
                            <div class="cert__row">
                                <div class="cert__img">
                                <a href="{{asset('storage/'.$review->image)}}" data-fancybox="reviews">
                                <img src="{{asset('storage/'.$review->image)}}" alt="">
                            </div>
                            </a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @if(count($reviews) == 0)
                    <div class="reviews__column">
                        <div class="reviews__cert cert">
                            <div class="cert__row">
                                <div class="cert__title">Сертификат</div>
                                <div class="cert__descr">
                                    <p>Основное отличие интерфейса поисковой выдачи от остальных — людям некогда всматриваться в него и оценивать внешний вид.</p>
                                    <p>Поведение человека здесь можно сравнить с поведением в аэропорту или на вокзале: нужно найти свой гейт или платформу, и взгляд сам находит указатель, сканирует его, выхватывает полезную информацию — и вот вы уже идёте по длинному коридору и даже не помните, какого цвета был тот самый указатель.</p>
                                    <p>Основное отличие интерфейса <br> поисковой выдачи от остальных него и оценивать внешний вид.</p>
                                </div>
                                <div class="cert__footer">
                                    <div class="cert__name">М.П.</div>
                                    <div class="cert__date date">Sep 8 2020</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="reviews__column">
                        <div class="reviews__cert cert">
                            <div class="cert__row">
                                <div class="cert__title">Сертификат</div>
                                <div class="cert__descr">
                                    <p>Основное отличие интерфейса поисковой выдачи от остальных — людям некогда всматриваться в него и оценивать внешний вид.</p>
                                    <p>Поведение человека здесь можно сравнить с поведением в аэропорту или на вокзале: нужно найти свой гейт или платформу, и взгляд сам находит указатель, сканирует его, выхватывает полезную информацию — и вот вы уже идёте по длинному коридору и даже не помните, какого цвета был тот самый указатель.</p>
                                    <p>Основное отличие интерфейса <br> поисковой выдачи от остальных него и оценивать внешний вид.</p>
                                </div>
                                <div class="cert__footer">
                                    <div class="cert__name">М.П.</div>
                                    <div class="cert__date date">Sep 8 2020</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <style>

    </style>
@endsection
