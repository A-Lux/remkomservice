@extends('layouts.app')
@section('content')
    <div class="contacts">
        <div class="container">
            <h1 class="contacts__title title">Контакты</h1>
            <div class="contacts__map">
                <a class="dg-widget-link" href="http://2gis.kz/almaty/firm/70000001030250478/center/76.89202308654787,43.24328015539814/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Алматы</a><div class="dg-widget-link"><a href="http://2gis.kz/almaty/center/76.892025,43.243103/zoom/16/routeTab/rsType/bus/to/76.892025,43.243103╎A-LUX, компания?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти проезд до A-LUX, компания</a></div><script charset="utf-8" src="https://widgets.2gis.com/js/DGWidgetLoader.js"></script><script charset="utf-8">new DGWidgetLoader({"width":814,"height":364,"borderColor":"#a3a3a3","pos":{"lat":43.24328015539814,"lon":76.89202308654787,"zoom":16},"opt":{"city":"almaty"},"org":[{"id":"70000001030250478"}]});</script><noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript>
                <div class="contacts__information">
                    {!! setting('site.contacts') !!}
                </div>
            </div>
        </div>
    </div>

    <style>
        .contacts{
            padding-bottom: 50px;
        }
    </style>
@endsection
