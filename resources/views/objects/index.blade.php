@extends('layouts.app')
@section('content')
    <div class="objects">
        <div class="container">
            <h2 class="objects__title title">Объекты</h2>
            <div class="objects__row">
                @foreach($objects as $object)
                <div class="objects__column">
                    <div class="objects__card card">
                        <a class="card__row">
                            <div class="card__img">
                            <img src="{{asset('storage/'.$object->thumbnail)}}" alt="" class="card__square">
                        </div>
                            <div class="card__text">
                                <div class="card__title">{{$object->name}}</div>
                                <div class="card__descr">
                                    {!! substr(strip_tags($object->description),0,100)!!}
                                </div>
                                <div class="card__date date">{{$object->created_at->month.'.'.$object->created_at->day.'.'.$object->created_at->year}}</div>
                            </div>
                        </a>
                    </div>
                </div>
                @endforeach
                @if(count($objects) == 0)
                <div class="objects__column">
                    <div class="objects__card card">
                        <a href="#" class="card__row">
                            <div class="card__img">
                            <img src="/images/icons/square2.jpg" alt="" class="card__square">
                        </div>
                            <div class="card__text">
                                <div class="card__title">People Like Meghan McCain <br>
                                    Are Why Americans Keep Dying</div>
                                <div class="card__descr">
                                    Why we want to believeWhy we want to believe Why we want to believe Why we believe Why we want to believeWhy we...
                                </div>
                                <div class="card__date">Sep 8 2020</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="objects__column">
                    <div class="objects__card card">
                        <a href="#" class="card__row">
                            <img src="/images/icons/square3.jpg" alt="" class="card__square">
                            <div class="card__text">
                                <div class="card__title">People Like Meghan McCain <br>
                                    Are Why Americans Keep Dying</div>
                                <div class="card__descr">
                                    Why we want to believeWhy we want to believe Why we want to believe Why we believe Why we want to believeWhy we...
                                </div>
                                <div class="card__date">Sep 8 2020</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="objects__column">
                    <div class="objects__card card">
                        <a href="#" class="card__row">
                            <img src="/images/icons/square1.jpg" alt="" class="card__square">
                            <div class="card__text">
                                <div class="card__title">People Like Meghan McCain <br>
                                    Are Why Americans Keep Dying</div>
                                <div class="card__descr">
                                    Why we want to believeWhy we want to believe Why we want to believe Why we believe Why we want to believeWhy we...
                                </div>
                                <div class="card__date">Sep 8 2020</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="objects__column">
                    <div class="objects__card card">
                        <a href="#" class="card__row">
                            <img src="/images/square4.jpg" alt="" class="card__square">
                            <div class="card__text">
                                <div class="card__title">People Like Meghan McCain <br>
                                    Are Why Americans Keep Dying</div>
                                <div class="card__descr">
                                    Why we want to believeWhy we want to believe Why we want to believe Why we believe Why we want to believeWhy we...
                                </div>
                                <div class="card__date">Sep 8 2020</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="objects__column">
                    <div class="objects__card card">
                        <a href="#" class="card__row">
                            <img src="/images/square5.jpg" alt="" class="card__square">
                            <div class="card__text">
                                <div class="card__title">People Like Meghan McCain <br>
                                    Are Why Americans Keep Dying</div>
                                <div class="card__descr">
                                    Why we want to believeWhy we want to believe Why we want to believe Why we believe Why we want to believeWhy we...
                                </div>
                                <div class="card__date">Sep 8 2020</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="objects__column">
                    <div class="objects__card card">
                        <a href="#" class="card__row">
                            <img src="/images/square6.jpg" alt="" class="card__square">
                            <div class="card__text">
                                <div class="card__title">People Like Meghan McCain <br>
                                    Are Why Americans Keep Dying</div>
                                <div class="card__descr">
                                    Why we want to believeWhy we want to believe Why we want to believe Why we believe Why we want to believeWhy we...
                                </div>
                                <div class="card__date">Sep 8 2020</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="objects__column">
                    <div class="objects__card card">
                        <a href="#" class="card__row">
                            <img src="/images/square7.jpg" alt="" class="card__square">
                            <div class="card__text">
                                <div class="card__title">People Like Meghan McCain <br>
                                    Are Why Americans Keep Dying</div>
                                <div class="card__descr">
                                    Why we want to believeWhy we want to believe Why we want to believe Why we believe Why we want to believeWhy we...
                                </div>
                                <div class="card__date">Sep 8 2020</div>
                            </div>
                        </a>
                    </div>
                </div>
                @endif
            </div>
            @if($objects->lastPage() == 1)
                <ul class="pagination pagination_equipment">


                    <li class="pagination__item pagination__item_active"><a href="#"></a>1</li>

                </ul>
            @else
                <ul class="pagination pagination_equipment">
                    @if($objects->currentPage() != 1)

                        <span onclick="window.location.href='{{$objects->path().'/?page='.($objects->currentPage()-1)}}'" class="pagination__prev"><img src="/images/icons/arrow.svg" alt="Назад"></span>

                    @endif
                    @for($i = $objects->currentPage();$i<=$objects->lastPage();$i++)
                        @if($i == $objects->currentPage() )
                            <li class="pagination__item pagination__item_active"><a href="#"></a>{{$i}}</li>
                            @elseif($i <=3)
                                <a href="{{$objects->path().'/?page='.$i}}"><li class="pagination__item">{{$i}}</li></a>
                           @elseif($i>3)
                                <span class="pagination__ext">..........</span>

                                <a href="{{$objects->path().'/?page='.$objects->lastPage()}}"><li class="pagination__item">{{$objects->lastPage()}}</li></a>


                        @endif



                    @endfor

                        @if($objects->currentPage() != $objects->lastPage())

                                <span class="pagination__next" onclick="window.location.href='{{$objects->path().'/?page='.($objects->currentPage()+1)}}'">

                                            <img src="/images/icons/arrow.svg" alt="Вперед">
                                </span>
                       @endif
                </ul>

            @endif


        </div>
    </div>
    <style>
        .pagination_equipment{
            padding-bottom: 10px;
        }
    </style>
@endsection
