@extends('layouts.app')

@section('content')
<div class="equipment">
    <div class="container">
        <h1 class="equipment__title title">Оборудование</h1>
        <div class="equipment__wrapper">
            <div class="equipment__row">
                @foreach($equipments as $equipment)
                <a href="{{route('vaporizer',$equipment->id)}}" class="equipment__column">
                    <div class="equipment__file file">
                        <div class="file__row">
                            <div class="file__img">
                                <img src="{{asset('storage/'.$equipment->thumbnail)}}" alt="">
                            </div>
                            <div class="file__right">
                            <div class="file__title">{{$equipment->name}}</div>
                            <div class="file__descr">{{$equipment->description}}</div>
                            <div class="file__next">
                                <img src="/images/icons/arrow-active.svg" alt="">
                            </div>
                        </div>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection

