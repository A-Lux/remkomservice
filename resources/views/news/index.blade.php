@extends('layouts.app')
@section('content')
    <div class="news">
        <div class="container">
            <h1 class="news__title title">Новости</h1>
            <div class="news__row">
                @foreach($news as $article)
                <div class="news__column" style="display: flex">
                    <div class="news__post post" style="display: flex; flex-direction: column;" >
                            <div class="post__image">
                                <img src="{{asset('storage/'.$article->thumbnail)}}" class="lazy" alt="">
                            </div>
                            <div class="post__content">
                                <div class="post__title">{{$article->title}}</div>
                                <div class="post__descr">
                                    {{strip_tags($article->description)}}
                                </div>
                                <div class="post__date date" style="margin-top: auto;">{{$article->created_at->month.'.'.$article->created_at->day.'.'.$article->created_at->year}}</div>
                            </div>
                    </div>
                </div>
                @endforeach
                @if(count($news)==0)
                <div class="news__column">
                    <div class="news__post post">
                        <a href="#" class="post__row">
                            <div class="post__image">
                                <img src="/images/news/2.jpg" class="lazy" alt="">
                            </div>
                            <div class="post__content">
                                <div class="post__title">People Like Meghan McCain
                                    Are Why Americans Keep Dying</div>
                                <div class="post__descr">Why we want to believeWhy we want to believeWhy we want to believeWhy we believeWhy we want to believeWhy we...</div>
                                <div class="post__date date">Sep 8 2020</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="news__column">
                    <div class="news__post post">
                        <a href="#" class="post__row">
                            <div class="post__image">
                                <img src="/images/news/3.jpg" class="lazy" alt="">
                            </div>
                            <div class="post__content">
                                <div class="post__title">People Like Meghan McCain
                                    Are Why Americans Keep Dying</div>
                                <div class="post__descr">Why we want to believeWhy we want to believeWhy we want to believeWhy we believeWhy we want to believeWhy we...</div>
                                <div class="post__date date">Sep 8 2020</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="news__column">
                    <div class="news__post post">
                        <a href="#" class="post__row">
                            <div class="post__image">
                                <img src="/images/news/4.jpg" class="lazy" alt="">
                            </div>
                            <div class="post__content">
                                <div class="post__title">People Like Meghan McCain
                                    Are Why Americans Keep Dying</div>
                                <div class="post__descr">Why we want to believeWhy we want to believeWhy we want to believeWhy we believeWhy we want to believeWhy we...</div>
                                <div class="post__date date">Sep 8 2020</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="news__column">
                    <div class="news__post post">
                        <a href="#" class="post__row">
                            <div class="post__image">
                                <img src="/images/news/5.jpg" class="lazy" alt="">
                            </div>
                            <div class="post__content">
                                <div class="post__title">People Like Meghan McCain
                                    Are Why Americans Keep Dying</div>
                                <div class="post__descr">Why we want to believeWhy we want to believeWhy we want to believeWhy we believeWhy we want to believeWhy we...</div>
                                <div class="post__date date">Sep 8 2020</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="news__column">
                    <div class="news__post post">
                        <a href="#" class="post__row">
                            <div class="post__image">
                                <img src="/images/news/6.jpg" class="lazy" alt="">
                            </div>
                            <div class="post__content">
                                <div class="post__title">People Like Meghan McCain
                                    Are Why Americans Keep Dying</div>
                                <div class="post__descr">Why we want to believeWhy we want to believeWhy we want to believeWhy we believeWhy we want to believeWhy we...</div>
                                <div class="post__date date">Sep 8 2020</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="news__column">
                    <div class="news__post post">
                        <a href="#" class="post__row">
                            <div class="post__image">
                                <img src="/images/news/7.jpg" class="lazy" alt="">
                            </div>
                            <div class="post__content">
                                <div class="post__title">People Like Meghan McCain
                                    Are Why Americans Keep Dying</div>
                                <div class="post__descr">Why we want to believeWhy we want to believeWhy we want to believeWhy we believeWhy we want to believeWhy we...</div>
                                <div class="post__date date">Sep 8 2020</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="news__column">
                    <div class="news__post post">
                        <a href="#" class="post__row">
                            <div class="post__image">
                                <img src="/images/news/8.jpg" class="lazy" alt="">
                            </div>
                            <div class="post__content">
                                <div class="post__title">People Like Meghan McCain
                                    Are Why Americans Keep Dying</div>
                                <div class="post__descr">Why we want to believeWhy we want to believeWhy we want to believeWhy we believeWhy we want to believeWhy we...</div>
                                <div class="post__date date">Sep 8 2020</div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="news__column">
                    <div class="news__post post">
                        <a href="#" class="post__row">
                            <div class="post__image">
                                <img src="/images/news/9.jpg" class="lazy" alt="">
                            </div>
                            <div class="post__content">
                                <div class="post__title">People Like Meghan McCain
                                    Are Why Americans Keep Dying</div>
                                <div class="post__descr">Why we want to believeWhy we want to believeWhy we want to believeWhy we believeWhy we want to believeWhy we...</div>
                                <div class="post__date date">Sep 8 2020</div>
                            </div>
                        </a>
                    </div>
                </div>
                @endif
            </div>
            @php $objects = $news; @endphp
            @if($news->lastPage() == 1)

                <ul class="pagination pagination_equipment">


                    <li class="pagination__item pagination__item_active"><a href="#"></a>1</li>

                </ul>
            @else
                <ul class="pagination pagination_equipment">
                    @if($objects->currentPage() != 1)

                        <span onclick="window.location.href='{{$objects->path().'/?page='.($objects->currentPage()-1)}}'" class="pagination__prev"><img src="/images/icons/arrow.svg" alt="Назад"></span>

                    @endif
                    @for($i = $objects->currentPage();$i<=$objects->lastPage();$i++)
                        @if($i == $objects->currentPage() )
                            <li class="pagination__item pagination__item_active"><a href="#"></a>{{$i}}</li>
                        @elseif($i <=3)
                            <a href="{{$objects->path().'/?page='.$i}}"><li class="pagination__item">{{$i}}</li></a>
                        @elseif($i>3)
                            <span class="pagination__ext">..........</span>

                            <a href="{{$objects->path().'/?page='.$objects->lastPage()}}"><li class="pagination__item">{{$objects->lastPage()}}</li></a>


                        @endif



                    @endfor

                    @if($objects->currentPage() != $objects->lastPage())

                        <span class="pagination__next" onclick="window.location.href='{{$objects->path().'/?page='.($objects->currentPage()+1)}}'">

                                            <img src="/images/icons/arrow.svg" alt="Вперед">
                                </span>
                    @endif
                </ul>

            @endif
        </div>
    </div>
    <style>
        .pagination_equipment{
            padding-bottom: 10px;
        }
    </style>

@endsection
