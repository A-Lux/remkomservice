@extends('layouts.app')
@section('content')
<div class="eq-inner">
    <div class="container">
        <h1 class="eq-inner__title title">
            {{ $equipment->name }}
        </h1>
        <div class="eq-inner__wrapper">
        <div class="eq-inner__row">
            @foreach($vaporizers as $vaporizer)
            <div class="eq-inner__column">
                <a href="{{route('vaporizerShow',$vaporizer->id)}}" class="eq-inner__item">
                    <div class="eq-inner__img">
                        <img src="/storage/{{ $vaporizer->image }}" alt="">
                    </div>
                    <div class="eq-inner__text">
                    <div class="eq-inner__titles">{{ $vaporizer->name }}</div>
                    <div class="eq-inner__descr">
                        {{substr($vaporizer->description,0,150) . "..." }}
                    </div>
                </div>
                    <div class="eq-inner__next">
                        <img src="/images/icons/arrow-active.svg" alt="">
                    </div>
                </a>
            </div>
            @endforeach
            {{-- <div class="eq-inner__column">
                <a href="#" class="eq-inner__item">
                    <div class="eq-inner__img">
                        <img src="/images/inner/2.png" alt="">
                    </div>
                    <div class="eq-inner__text">
                    <div class="eq-inner__titles">Электрические испарители</div>
                    <div class="eq-inner__descr">Электрические испарители сжиженного углеводородного газа FAS 2000 немецкого производства используются в системах автономного и резервного газоснабжения.</div>
                </div>
                    <div class="eq-inner__next">
                        <img src="/images/icons/arrow-active.svg" alt="">
                    </div>
                </a>
                </div> --}}
            </div>
        </div>
    </div>
</div>
@endsection
