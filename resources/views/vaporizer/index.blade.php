@extends('layouts.app')
@section('content')
<div class="eq-side">
    <div class="container">
        <h1 class="eq-side__title title">Оборудования</h1>
        <div class="eq-side__wrapper">
            <div class="eq-side__main-item main-item">
                <div class="main-item__row">
                    <div class="main-item__img">
                        <img src="{{asset('storage/'.$vaporizer->image)}}" alt="">
                    </div>
                    <div class="main-item__text">
                        <div class="main-item__title">{{ $vaporizer->name }}</div>
                        <div class="main-item__descr">
                            {{ $vaporizer->description }}
                        </div>
                        <div class="main-item__icons">
                            @foreach(json_decode($vaporizer->documents) as $document)
                                <a href="/storage/{{ $document->download_link }}" target="_blank" class="main-item__icon">
                            <img src="/images/icons/pdf.png" alt="">
                            <div class="main-item__icon-descr">{{$document->original_name}}</div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="eq-side__sliders">
            <div class="eq-side__slider">
                @foreach(json_decode($vaporizer->slides) as $slide)
                <div class="eq-side__slide">
                    <a href="{{asset('storage/'.$slide)}}" data-fancybox="certificates">
                    <img src="{{asset('storage/'.$slide)}}" alt="">
                </a>
                </div>
            @endforeach
            </div>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection
