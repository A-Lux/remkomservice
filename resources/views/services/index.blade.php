@extends('layouts.app')
@section('content')
    <div class="services">
        <div class="container">
            @if(setting('site.show_banner') == 1)
            <div class="services__image">

                <img src="{{setting('site.service_banner') != null ? asset('storage/'.setting('site.service_banner')) :"/images/services/1.jpg"}}" alt="Баннер">
            </div>
            @endif
            <h1 class="services__title">Услуги</h1>
            <div class="services__subtitle">{{setting('site.service_text') != null ? setting('site.service_text') : 'Just send your design files to our developer team in the file format of your choice: Photoshop, Sketch, Adobe XD, or even Figma.
                Just send your design files to our developer team in the file'}}</div>
            @foreach($services as $service)
            <div class="services__image">
                <img src="{{asset('storage/'.$service->image)}}" alt="Баннер" height="450px">
            </div>
            <h2 class="services__title">{{$service->title}}</h2>
            <div class="services__subtitle">{{$service->description}}</div>
            @endforeach
            @if(count($services) == 0)
            <div class="services__image">
                <img src="/images/services/3.jpg" alt="Баннер">
            </div>
            <h2 class="services__title">Подготовка оборудования</h2>
            <div class="services__subtitle">Just send your design files to our developer team in the file format of your choice: Photoshop, Sketch, Adobe XD, or even Figma.
                Just send your design files to our developer team in the file</div>
            <div class="services__image">
                <img src="/images/services/4.jpg" alt="Баннер">
            </div>
            <h2 class="services__title">Деятельность компании</h2>
            <div class="services__subtitle">Just send your design files to our developer team in the file format of your choice: Photoshop, Sketch, Adobe XD, or even Figma.
                Just send your design files to our developer team in the file</div>
            <div class="services__image">
                <img src="/images/services/5.jpg" alt="Баннер">
            </div>
            <h2 class="services__title">Монтаж</h2>
            <div class="services__subtitle">Just send your design files to our developer team in the file format of your choice: Photoshop, Sketch, Adobe XD, or even Figma.
                Just send your design files to our developer team in the file</div>
            <div class="services__image">
                <img src="/images/services/6.jpg" alt="Баннер">
            </div>
            <h2 class="services__title">Проекты</h2>
            <div class="services__subtitle">Just send your design files to our developer team in the file format of your choice: Photoshop, Sketch, Adobe XD, or even Figma.
                Just send your design files to our developer team in the file</div>
            @endif
        </div>
    </div>
    </div>
@endsection
