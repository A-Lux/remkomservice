<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\Front\MainController;

Route::get('/',[MainController::class,'index'])->name('index');
Route::get('/services',[MainController::class,'services'])->name('services');
Route::get('/objects',[MainController::class,'objects'])->name('objects');
Route::get('/equipment',[MainController::class,'equipment'])->name('equipment');
Route::get('/equipment/category/{id}',[MainController::class,'vaporizer'])->name('vaporizer');
Route::get('/equipment/category/show/{id}',[MainController::class,'vaporizerShow'])->name('vaporizerShow');
Route::get('/equipment/show/{id}',[MainController::class,'equipmentShow'])->name('equipmentShow');

Route::get('/news',[MainController::class,'news'])->name('news');
Route::get('/ours',[MainController::class,'ours'])->name('ours');
Route::get('/contacts',[MainController::class,'contacts'])->name('contacts');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
