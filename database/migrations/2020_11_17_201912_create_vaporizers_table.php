<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVaporizersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vaporizers', function (Blueprint $table) {
            $table->id();
            $table->integer('order')->default(1);
            $table->string('name');
            $table->text('description');
            $table->string('image');
            $table->string('slides');
            $table->string('documents')->nullable();
            $table->string('document_texts')->nullable();
            $table->foreignId('category_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vaporizers');
    }
}
