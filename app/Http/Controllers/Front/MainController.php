<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Advantage;
use App\Models\Banner;
use App\Models\BuildingObject;
use App\Models\Equipment;
use App\Models\News;
use App\Models\Review;
use App\Models\Service;
use App\Models\Vaporizer;
use Illuminate\Http\Request;

class MainController extends Controller
{
    //
    public function index()
    {
        $banners = Banner::orderBy('id','asc')->limit(3)->get();
        $advantages = Advantage::orderBy('id','asc')->limit(4)->get();
        $objects = BuildingObject::orderBy('id','asc')->limit(3)->get();
        $news = News::orderBy('id','asc')->limit(3)->get();
        $reviews = Review::orderBy('id','asc')->limit(3)->get();
        return view('welcome',compact(['banners','objects','news','reviews','advantages']));
    }

    public function services()
    {
        $services = Service::orderBy('id','asc')->get();
        return view('services.index',compact(['services']));
    }

    public function objects()
    {
        $objects = BuildingObject::orderBy('id','asc')->paginate(3);
        return view('objects.index',compact(['objects']));
    }

    public function equipment()
    {
        $equipments = Equipment::orderBy('id','asc')->paginate(3);
        return view('equipment.index',compact(['equipments']));
    }
    public function equipmentShow($id)
    {
        $equipment = Equipment::find($id);
        return view('equipment-inner',compact(['equipment']));
    }
    public function vaporizer(Request $request, $equipment)
    {
        $vaporizers = Vaporizer::where('equipment_id', $equipment)->paginate(3);
        $equipment = Equipment::find($equipment);
        return view('vaporizer.all',compact([
            'vaporizers',
            'equipment'
            ]));
    }

    public function vaporizerShow($id)
    {
        $vaporizer = Vaporizer::find($id);
        return view('vaporizer.index',compact(['vaporizer']));
    }
    public function news()
    {

        $news = News::orderBy('id','asc')->paginate(3);
        return view('news.index',compact(['news']));
    }

    public function ours()
    {
        return view('ours.index');
    }

    public function contacts()
    {
        return view('contacts.index');
    }
}
